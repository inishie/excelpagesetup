
**excelPageSetup**

	Excelのヘッダー/フッターを変更

---

[TOC]

---

## Usage

	ヘッダーやフッターを変更したいExcelファイルをvbsの引数に指定して起動します。
	ドラッグ＆ドロップ操作用にバッチファイルを追加しています。
	ex) cscript //nologo excelPageSetup.vbs C:/Test.xlsx

	変更条件や文字列は iniファイル で指定できます。
	excelPageSetupConfig.ini
		各表示位置の処理フラグ (0=何もしない, 10=消す, 21=必ず上書き, 22=値がある場合に上書き, 23=値が無い場合に設定)
		LeftHeaderFlg : 左上のフラグ
		CenterHeaderFlg : 上のフラグ
		RightHeaderFlg : 右上のフラグ
		LeftFooterFlg : 左下のフラグ
		CenterFooterFlg : 下のフラグ
		RightFooterFlg : 右下のフラグ

		指定する場合の文字列は以下の変数に設定。
		LeftHeaderText : 左上の文字
		CenterHeaderText : 上の文字
		RightHeaderText : 右上の文字
		LeftFooterText : 左下の文字
		CenterFooterText : 下の文字
		RightFooterText : 右下の文字


## Note

	・vbs, bat, ini は日本語を含むため、ファイルのエンコードはsjisを前提としています。
	・実行端末に Microsoft Office Excel が必要です。
	・ドキュメントのcopyrightなどの統一や一括変換を想定してツールです。


---

## References

	REONTOSANTA
	VBScriptでExcelを操作する - REONTOSANTA
	https://knowledge.reontosanta.com/archives/838

	Microsoft
	ヘッダーとフッターに指定できる書式コードと VBA コード | Microsoft Docs
	https://docs.microsoft.com/ja-jp/office/vba/excel/concepts/workbooks-and-worksheets/formatting-and-vba-codes-for-headers-and-footers


## Author

	inishie
	inishie77@hotmail.com
	https://bitbucket.org/inishie/

## License

Copyright (c) 2022 inishie.  
Released under the [MIT license](https://en.wikipedia.org/wiki/MIT_License).
