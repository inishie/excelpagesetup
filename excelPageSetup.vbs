Option Explicit
' ExcelのPageSetup属性変更
' v 1.02
' ex) cscript //nologo excelPageSetup.vbs C:/Test.xlsx

Dim objFileSys, oXlsApp, rootPath, strPara1, excel, sCount, idx, oSheet
Dim LeftHeaderFlg,  CenterHeaderFlg,  RightHeaderFlg,  LeftFooterFlg,  CenterFooterFlg,  RightFooterFlg
Dim LeftHeaderText, CenterHeaderText, RightHeaderText, LeftFooterText, CenterFooterText, RightFooterText

If WScript.Arguments.Count <> 1 Then
	WScript.Echo "パラメータ不備"
	WScript.Quit 1
End If

Set objFileSys = CreateObject("Scripting.FileSystemObject")
If objFileSys Is Nothing Then
	' FileSystemObject起動失敗
	WScript.Echo "FileSystemObject起動失敗"
	WScript.Quit 1
End If

rootPath = objFileSys.getParentFolderName(WScript.ScriptFullName) & "\"

' 各表示位置の処理フラグ(0=何もしない, 10=消す, 21=必ず上書き, 22=値がある場合に上書き, 23=値が無い場合に設定)
' 設定値読込
ExecuteGlobal objFileSys.OpenTextFile(rootPath & "excelPageSetupConfig.ini").ReadAll()

strPara1 = WScript.Arguments(0)

' Excel起動
Set oXlsApp = CreateObject("Excel.Application")
If oXlsApp Is Nothing Then
	' Excel起動失敗
	WScript.Echo "Excel起動失敗"
	Set objFileSys = Nothing
	WScript.Quit 1
End If

' Excel起動成功
oXlsApp.Application.Visible = False			' --Excel表示（falseにすると非表示にできる）
oXlsApp.Application.DisplayAlerts = False	' --Excelの警告を非表示にする
' WScript.Sleep(1000)						' --1秒待つ

On Error Resume Next ' 強行
Set excel = oXlsApp.Application.Workbooks.Open(strPara1,0,,,,,True,,,,,,,,1)		' --ブックを開く(リンク更新せず,読み取り選択を無視,復旧込み)
If Err.Number <> 0 Then
	' ファイルに問題あり
	WScript.Echo "編集失敗: " & strPara1
	Set objFileSys = Nothing
	WScript.Quit 1
End If
On Error Goto 0 ' 解除

sCount = excel.Sheets.Count

For idx = 1 To sCount
	Set oSheet = excel.Worksheets(idx)		' --シート選択

	oSheet.PageSetup.LeftHeader   = updatePageSetup(oSheet.PageSetup.LeftHeader,   LeftHeaderFlg,   LeftHeaderText)		'左上
	oSheet.PageSetup.CenterHeader = updatePageSetup(oSheet.PageSetup.CenterHeader, CenterHeaderFlg, CenterHeaderText)	'上
	oSheet.PageSetup.RightHeader  = updatePageSetup(oSheet.PageSetup.RightHeader,  RightHeaderFlg,  RightHeaderText)	'右上
	oSheet.PageSetup.LeftFooter   = updatePageSetup(oSheet.PageSetup.LeftFooter,   LeftFooterFlg,   LeftFooterText)		'左下
	oSheet.PageSetup.CenterFooter = updatePageSetup(oSheet.PageSetup.CenterFooter, CenterFooterFlg, CenterFooterText)	'下
	oSheet.PageSetup.RightFooter  = updatePageSetup(oSheet.PageSetup.RightFooter,  RightFooterFlg,  RightFooterText)	'右下

Next

excel.Saveas(strPara1)

Set excel = Nothing

' --Excel終了
oXlsApp.Quit

WScript.echo strPara1

' --Excelオブジェクトクリア
Set oXlsApp = Nothing
Set objFileSys = Nothing
WScript.Quit 0

'ページ設定の変更
Function updatePageSetup(ByVal style, ByVal flg, ByVal Text)
	updatePageSetup = style
	If flg = 0 Then
		'何もしない
	End If
	If flg = 10 Then
		'消す
		updatePageSetup = ""
	End If
	If flg = 21 Then
		'必ず上書き
		updatePageSetup = Text
	End If
	If flg = 22 And updatePageSetup <> "" Then
		'値がある場合に上書き
		updatePageSetup = Text
	End If
	If flg = 23 And updatePageSetup = "" Then
		'値が無い場合に設定
		updatePageSetup = Text
	End If

End Function
